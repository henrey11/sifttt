import numpy as np
from sklearn.linear_model import LinearRegression
from datetime import datetime
from mysql.connector import connect
from math import pi

def new_decPattern(new_values, last_state):
    if len(new_values) == 0:
        event = last_state[0]
        state = last_state[1]
        if event > 0:
            event = 0
            state['event']=0
            state['time']=''

        return event, state, last_state[2], last_state[3]

    if last_state:
        event = last_state[0]
        state = last_state[1]
        value = last_state[2]
        parameter = last_state[3]
        if event > 0:
            event = 0
            state['event']=0
            state['time']=''
        for i in new_values:
            if i['type'] == 'data':
                if len(value[1]) < int(parameter['amount']):
                    value[1].append(i['value'])
                if len(value[1]) == int(parameter['amount']):
                    x = np.array(value[0]).reshape((-1, 1))
                    y = np.array(value[1])
                    model = LinearRegression().fit(x, y)
                    yPred = model.predict(x)
                    angle = np.arctan2(yPred[-1]-yPred[0],value[0][-1]-value[0][0]) * 180 / pi
                    if angle <= (-float(parameter['decreasing']) + float(parameter['tolerance'])) and angle >= (-float(parameter['decreasing']) - float(parameter['tolerance'])) and angle <= 0:
                        event = event + 1
                        state['event']=value[1][0]
                        state['time']=i['time']
                        value[1] = value[1][-1:]
                    else:
                        value[1].pop(0)
            else:   # type = delete
                return None
    else:
        if new_values[0]['type'] == 'configs':
            event = 0
            state = {}
            value = []
            parameter = new_values[0]['parameter']
            temp = []
            for i in range(1,int(parameter['amount'])+1):
                temp.append(i)
            value.append(temp)
            value.append([])
        else:
            return None

    #last_state 0   ,   1   ,   2   ,   3
    return event, state, value, parameter
