from __future__ import print_function
import json
import sys
from mysql.connector import connect
from datetime import datetime
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
from new_inc import new_incPattern
from new_dec import new_decPattern
import numpy as np
from sklearn.linear_model import LinearRegression
import multiprocessing


def storePattern(iter):
    val=[]
    for record in iter:
        temp = (record[1][3]['trigger_id'],record[0],record[1][1]['event'],record[1][1]['time'],datetime.now().isoformat())
        val.append(temp)
    if len(val) > 0:
        mydb = connect(
            host="134.208.2.171",
            port=3306,
            user="root",
            passwd="ndhu",
            database="tempdb"
        )
        mycursor = mydb.cursor()
        sql = "INSERT INTO events (trigger_id,data_id,1stvalue,time1,time_str) VALUES (%s,%s,%s,%s,%s)"
        mycursor.executemany(sql,val)
        mydb.commit()
        mycursor.close()
        mydb.close()


if __name__ == "__main__":

    sc = SparkContext(appName="IoT")
    sc.addPyFile("new_inc.py")
    sc.addPyFile("new_dec.py")
    ssc = StreamingContext(sc, 1)
    ssc.checkpoint("checkpoint")

    kafkaStream = KafkaUtils.createDirectStream(ssc, ["sifttt1",], {"bootstrap.servers": "192.168.50.3:9092,192.168.50.4:9092,192.168.50.5:9092,192.168.50.8:9092,192.168.50.9:9092"}) \
                    .map(lambda x: x[1]) \
                    .map(lambda x: json.loads(x))

    kafkaStream.count()
    #kafkaStream.pprint()

    #increase + configs
    increase = kafkaStream.filter(lambda x: 'new_inc' in x['pattern']) \
                        .map(lambda x: (x['id'], x['data'])) \
                        .updateStateByKey(new_incPattern)

    decrease = kafkaStream.filter(lambda x: 'new_dec' in x['pattern']) \
                        .map(lambda x: (x['id'], x['data'])) \
                        .updateStateByKey(new_decPattern)

    storeDb = increase.union(decrease) \
                    .filter(lambda x: x[1][0] > 0) \
                    .foreachRDD(lambda rdd: rdd.foreachPartition(storePattern))

    ssc.start()
    ssc.awaitTermination()
