import uuid
import json
import requests
import time
import datetime

url = 'http://134.208.2.171/notifications'
uuidTemp = str(uuid.uuid4())
headers = { 'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'Accept-Encoding': 'gzip, deflate',
            'Content-Type': 'application/json',
            'X-Request-ID': uuidTemp}

fhandle = open('testData.txt',"r")
lines = fhandle.readlines()
fhandle.close()

for i in range(len(lines)):
    print('time: ', datetime.datetime.now())
    temp=json.loads(lines[i])
    temp['time']=str(datetime.datetime.now())
    temp=str(temp)
    data = json.dumps({'data': temp}, ensure_ascii=False).encode('utf-8')
    print(temp)
    res = requests.post(url, headers=headers, data=data)
    print('response:'+str(res))
    if str(res) == "<Response [200]>":
        print("OK")
    time.sleep(1)
