# -*- coding: utf-8 -*-
#flask
from flask import Blueprint, request, jsonify ,render_template_string
from dateutil import parser
import datetime
import json
import operator
import re
import time
import traceback
import threading
import multiprocessing
import requests
import uuid
import mysql.connector #mysql
from kafka import KafkaProducer
from confluent_kafka import Producer

#producer = KafkaProducer(bootstrap_servers=['134.208.2.163:9092','134.208.2.163:9093','134.208.2.163:9094','134.208.2.163:9098','134.208.2.163:9097'])

conf = {'bootstrap.servers':"134.208.2.163:9092,134.208.2.163:9093,134.208.2.163:9094,134.208.2.163:9098,134.208.2.163:9097"}
producer = Producer(conf)

#main
ssifttt = Blueprint('ssifttt', __name__)
#ssifttt.wsgi_app = HTTPMethodOverride(ssifttt.wsgi_app)

IFTTT_SERVICE_KEY = "kpax4OO3o76S-wWt4u16KurI9DOBuOY_f0t08Bb-8LIxjauRmCM-ETGcHz7v804o"
trigger_id=""

@ssifttt.route('/ifttt/v1/status')
def status():
    method = getattr(IftttController(), "status")
    return method()

@ssifttt.route('/ifttt/v1/test/setup', methods=['POST'])
def setup():
    method = getattr(IftttController(), "setup")
    return method()

@ssifttt.route('/ifttt/v1/triggers/<string:pattern>', methods=['POST'])
def pattern_detected(pattern):
    method = getattr(IftttController(), "pattern_detected")
    return method(pattern)

@ssifttt.route('/ifttt/v1/triggers/<string:pattern>/trigger_identity/<string:trigger_id>', methods=['DELETE'])
def delete_trigger_id(pattern, trigger_id):
    method = getattr(IftttController(), "delete_trigger_id")
    return method(pattern, trigger_id)

@ssifttt.route('/ifttt/v1/triggers/<string:pattern>/fields/data_id/options', methods=['POST'])
def data_id_options(pattern):
    method = getattr(IftttController(), "data_id_options")
    return method(pattern)

@ssifttt.route('/ifttt/v1/triggers/new_inc/fields/increasing/validate', methods=['POST'])
def new_inc_percentage_validation():
    method = getattr(IftttController(), "new_inc_percentage_validation")
    return method()

@ssifttt.route('/ifttt/v1/triggers/new_inc/fields/tolerance/validate', methods=['POST'])
def new_inc_percentage_of_tolerance_validation():
    method = getattr(IftttController(), "new_inc_percentage_of_tolerance_validation")
    return method()

@ssifttt.route('/ifttt/v1/triggers/new_inc/fields/amount/validate', methods=['POST'])
def new_inc_amount_validation():
    method = getattr(IftttController(), "new_inc_amount_validation")
    return method()

@ssifttt.route('/ifttt/v1/triggers/new_dec/fields/decreasing/validate', methods=['POST'])
def new_dec_percentage_validation():
    method = getattr(IftttController(), "new_dec_percentage_validation")
    return method()

@ssifttt.route('/ifttt/v1/triggers/new_dec/fields/tolerance/validate', methods=['POST'])
def new_dec_percentage_of_tolerance_validation():
    method = getattr(IftttController(), "new_dec_percentage_of_tolerance_validation")
    return method()

@ssifttt.route('/ifttt/v1/triggers/new_dec/fields/amount/validate', methods=['POST'])
def new_dec_amount_validation():
    method = getattr(IftttController(), "new_dec_amount_validation")
    return method()

@ssifttt.route('/ifttt/v1/actions/send_command', methods=['POST'])
def send_command():
    method = getattr(IftttController(), "send_command")
    return method()

@ssifttt.route('/ifttt/v1/actions/send_command/fields/device_id/options', methods=['POST'])
def device_id_options():
    method = getattr(IftttController(), "device_id_options")
    return method()

#ssifttt.secret_key = '3v\x9c1\xb1\xa6[\xd8\xae\x91%\x8a7\xf3.\x18i[u\x9d\xc8\x88\x13\x04'

class IftttController():
    @ssifttt.before_request
    def __return_errors_unless_valid_service_key():
        if request.headers.get("IFTTT_SERVICE_KEY") is None:
            mydb = mysql.connector.connect(
                host="localhost",
                user="root",
                passwd="ndhu",
                database="tempdb"
            )
            mycursor = mydb.cursor()
            mycursor.execute("SELECT access_token FROM token")
            tempToken = mycursor.fetchall()
            access_token = request.headers.get('Authorization')
            access_token = access_token.split(' ')
            if access_token[1] not in str(tempToken):
                return json.dumps({'errors' : [{'message': 'Access Token Denied!!!'}]}, ensure_ascii=False).encode('utf-8'), 401
        elif request.headers["IFTTT_SERVICE_KEY"] != IFTTT_SERVICE_KEY:
            return json.dumps({'errors' : [{'message': 'Invalid Channel Key!!!'}]}, ensure_ascii=False).encode('utf-8'), 401

    @ssifttt.before_request
    def __return_errors_unless_valid_action_fields():
        if '/ifttt/v1/triggers' in request.path:
            tempPath = request.path
            tempPath = tempPath.split('/')
            if len(tempPath) == 5:
                mydb = mysql.connector.connect(
                    host="localhost",
                    user="root",
                    passwd="ndhu",
                    database="tempdb"
                )
                mycursor = mydb.cursor()
                sql = "SELECT field FROM trigger_field WHERE pattern=%s"
                temp = (tempPath[-1],)
                mycursor.execute(sql,temp)
                fields = mycursor.fetchall()
                try:
                    temp = request.json['triggerFields']
                    temp = request.json['triggerFields']['data_id']
                    for field in fields:
                        temp = request.json['triggerFields'][str(field[0])]
                except:
                     return json.dumps({'errors' : [{'status': 'SKIP', 'message': 'Missing Trigger Fields!!!'}]}, ensure_ascii=False).encode('utf-8'), 400
        elif request.path == '/ifttt/v1/actions/send_command':
            try:
                temp = request.json['actionFields']
                temp = request.json['actionFields']['device_id']
                temp = request.json['actionFields']['command_message']
            except:
                return json.dumps({'errors' : [{'status': 'SKIP', 'message': 'Missing Action Fields!!!'}]}, ensure_ascii=False).encode('utf-8'), 400

    def status(self):
        return jsonify(''), 200

    def setup(self):
        return json.dumps({'data':{'accessToken':'QjXJN3MzvjREQuG9Dh0SdIAyfamHKN',
            'samples': {'triggers': {'new_inc': {'data_id': 'WA', 'increasing': '10',
            'tolerance': '3', 'amount': '5'}, 'new_dec': {'data_id': 'WA', 'decreasing': '10',
            'tolerance': '3', 'amount': '5'}, 'vshape': {'data_id': 'WA', 'decreasing': '11', 'tolerance1': '2', 'times1': '5',
            'increasing': '10', 'tolerance2': '1', 'times2': '10'}, 'inv_vshape': {'data_id': 'WA',
            'increasing': '11', 'tolerance1': '2', 'times1': '5', 'decreasing': '10', 'tolerance2': '1',
             'times2': '10'}}, 'actions':{'send_command':{'device_id':'WA', 'command_message':'REDON'}},
            'triggerFieldValidations': {'new_inc': {'increasing': {'valid':'10','invalid':'92'},
            'tolerance': {'valid':'3','invalid':'a'}, 'amount': {'valid':'5','invalid':'a'}},
            'new_dec': {'decreasing': {'valid':'10','invalid':'a'},'tolerance': {'valid':'3','invalid':'a'},
            'amount': {'valid':'5','invalid':'a'}}}}}}, ensure_ascii=False).encode('utf-8')

    #pattern_detected
    def pattern_detected(self, pattern):
        datatemp = []
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="ndhu",
            database="tempdb"
        )
        mycursor = mydb.cursor()
        sql = "SELECT * FROM trigger_pattern WHERE name=%s"
        temp = (pattern,)
        mycursor.execute(sql,temp)
        patternRegistered = mycursor.fetchone()
        if patternRegistered is not None:
            trigger_id = request.json.get("trigger_identity")
            sql = "SELECT * FROM " + str(pattern) + "_setting WHERE trigger_id=%s"
            temp = (trigger_id,)
            mycursor.execute(sql,temp)
            triggerRegistered = mycursor.fetchone()
            if triggerRegistered is None:
                tempConf = dict()
                data = dict()
                data_id = request.json['triggerFields']['data_id']
                tempFields = request.json['triggerFields']
                tempFields['trigger_id'] = trigger_id
                #del tempFields['data_id']
                tempConf['parameter'] = tempFields
                tempConf['type'] = 'configs'
                data['id'] = data_id
                data['pattern'] = [pattern]
                data['data'] = tempConf
                sql = "SELECT field FROM trigger_field WHERE pattern=%s"
                temp = (pattern,)
                mycursor.execute(sql,temp)
                triggerFields = mycursor.fetchall()
                sql1 = "INSERT INTO " + str(pattern) + "_setting (trigger_id,data_id"
                sql2 = " VALUES (%s, %s"
                temp = (trigger_id, data_id)
                fieldTemp = []
                for triggerField in triggerFields:
                    sql1 = sql1 + "," + triggerField[0]
                    sql2 = sql2 + ", %s"
                    fieldTemp.append(request.json['triggerFields'][str(triggerField[0])])
                sql1 = sql1 + ")"
                sql2 = sql2 + ")"
                sql = sql1 + sql2
                temp = temp + tuple(fieldTemp)
                mycursor.execute(sql,temp)
                mydb.commit()
                sql = "UPDATE data_setting SET " + str(pattern) + "= 1 WHERE data_id = %s"
                temp = (data_id,)
                mycursor.execute(sql,temp)
                mydb.commit()
                data = json.dumps(data, ensure_ascii=False).encode('utf-8')
                #key=pattern.encode('utf-8')
                #future=producer.send('sifttt1', value=data)
                #result=future.get(timeout=0)
                producer.produce('sifttt1',value=data)
                producer.poll(timeout=0)

            limit = request.json.get("limit")
            if limit is None:
                limit=50
            else:
                limit = int(limit)

            sql = "SELECT * FROM events WHERE trigger_id = %s ORDER BY created_at DESC LIMIT %s"
            temp = (trigger_id, limit)
            mycursor.execute(sql,temp)
            myresult = mycursor.fetchall()
            for id,trigger_id,data_id,value1,value,time1,time_str,date in myresult:
                created_at = date.isoformat()
                created_at = created_at + 'Z'
                secondTemp = datetime.datetime.strptime(str(date), "%Y-%m-%d %H:%M:%S")
                second = time.mktime(secondTemp.timetuple())
                second = int(second)
                x={'created_at' : created_at,'data_id': data_id,
                    'meta': {'id': id, 'timestamp': second}}
                datatemp.append(x)
        mycursor.close()
        mydb.close()
        return json.dumps({'data': datatemp}, ensure_ascii=False).encode('utf-8')

    def delete_trigger_id(self, pattern, trigger_id):
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="ndhu",
            database="tempdb"
        )
        mycursor = mydb.cursor()
        sql = "SELECT data_id from " + str(pattern) + "_setting WHERE trigger_id = %s"
        temp = (trigger_id,)
        mycursor.execute(sql,temp)
        data_id = mycursor.fetchone()
        sql = "DELETE FROM " + str(pattern) + "_setting WHERE trigger_id = %s"
        mycursor.execute(sql,temp)
        mydb.commit()
        sql = "UPDATE data_setting SET " + str(pattern) +" = 0 WHERE data_id = %s"
        temp = (data_id[0],)
        mycursor.execute(sql,temp)
        mydb.commit()
        mycursor.close()
        mydb.close()
        data = dict()
        tempDel = dict()
        tempDel['type'] = 'delete'
        data['id'] = data_id[0]
        data['pattern'] = [pattern]
        data['data'] = tempDel
        data = json.dumps(data, ensure_ascii=False).encode('utf-8')
        #key=pattern.encode('utf-8')
        #future=producer.send('sifttt1', value=data)
        #result=future.get(timeout=0)
        producer.produce('sifttt1',value=data)
        producer.poll(timeout=0)
        return jsonify(''), 200

    def data_id_options(self, pattern):
        access_token = request.headers.get('Authorization')
        access_token = access_token.split(' ')
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="ndhu",
            database="tempdb"
        )
        mycursor = mydb.cursor()
        sql = "SELECT * FROM token WHERE access_token = %s"
        temp = (access_token[1],)
        mycursor.execute(sql,temp)
        myresult = mycursor.fetchone()
        sql = "SELECT * FROM user WHERE id = %s"
        temp = (myresult[2],)
        mycursor.execute(sql,temp)
        myresult = mycursor.fetchone()
        sql = "SELECT * FROM data_setting WHERE email = %s AND " + str(pattern) + " = 0"
        temp = (myresult[1],)
        mycursor.execute(sql,temp)
        myresult = mycursor.fetchall()
        datatemp = []
        for i in myresult:
            x={'label' : i[2],'value': i[2]}
            datatemp.append(x)
        mycursor.close()
        mydb.close()
        return json.dumps({'data': datatemp}, ensure_ascii=False).encode('utf-8')

    #new_inc
    def new_inc_percentage_validation(self):
        temp = request.json.get("value")
        try:
            temp = float(temp)
            if temp >= 0 and temp <= 90:
                return json.dumps({'data':{'valid': True}}, ensure_ascii=False).encode('utf-8')
            else:
                return json.dumps({'data':{'valid': False,'message':'Sorry, the value entered is not in range of 0-90.'}})
        except:
            return json.dumps({'data':{'valid': False,'message':'Sorry, the value entered is not in numerical.'}})

    def new_inc_percentage_of_tolerance_validation(self):
        temp = request.json.get("value")
        try:
            temp = float(temp)
            if temp >= 0 and temp <= 90:
                return json.dumps({'data':{'valid': True}}, ensure_ascii=False).encode('utf-8')
            else:
                return json.dumps({'data':{'valid': False,'message':'Sorry, the value entered is not in range of 0-90.'}})
        except:
            return json.dumps({'data':{'valid': False,'message':'Sorry, the value entered is not in numerical.'}})

    def new_inc_amount_validation(self):
        temp = request.json.get("value")
        try:
            temp = int(temp)
            return json.dumps({'data':{'valid': True}}, ensure_ascii=False).encode('utf-8')
        except:
            return json.dumps({'data':{'valid': False,'message':'Sorry, the value entered is not in integer.'}})

    #new_dec validation
    def new_dec_percentage_validation(self):
        temp = request.json.get("value")
        try:
            temp = float(temp)
            if temp >= 0 and temp <= 90:
                return json.dumps({'data':{'valid': True}}, ensure_ascii=False).encode('utf-8')
            else:
                return json.dumps({'data':{'valid': False,'message':'Sorry, the value entered is not in range of 0-90.'}})
        except:
            return json.dumps({'data':{'valid': False,'message':'Sorry, the value entered is not in numerical.'}})

    def new_dec_percentage_of_tolerance_validation(self):
        temp = request.json.get("value")
        try:
            temp = float(temp)
            if temp >= 0 and temp <= 90:
                return json.dumps({'data':{'valid': True}}, ensure_ascii=False).encode('utf-8')
            else:
                return json.dumps({'data':{'valid': False,'message':'Sorry, the value entered is not in range of 0-90.'}})
        except:
            return json.dumps({'data':{'valid': False,'message':'Sorry, the value entered is not in numerical.'}})

    def new_dec_amount_validation(self):
        temp = request.json.get("value")
        try:
            temp = int(temp)
            return json.dumps({'data':{'valid': True}}, ensure_ascii=False).encode('utf-8')
        except:
            return json.dumps({'data':{'valid': False,'message':'Sorry, the value entered is not in integer.'}})

    #action part
    def send_command(self):
        datatemp = []
        device_id = request.json['actionFields']['device_id']
        command_message = request.json['actionFields']['command_message']
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="ndhu",
            database="tempdb"
        )
        mycursor = mydb.cursor()
        sql = "INSERT INTO command (device_id,command_message,time_str) VALUES (%s, %s, %s)"
        time_str=datetime.datetime.now().isoformat()
        temp = (device_id, command_message,time_str)
        mycursor.execute(sql,temp)
        mydb.commit()
        sql = "SELECT id from data_setting WHERE data_id = %s"
        temp = (device_id,)
        mycursor.execute(sql,temp)
        id = mycursor.fetchone()
        mycursor.close()
        mydb.close()
        x={'id' : id[0]}
        datatemp.append(x)
        return json.dumps({'data': datatemp}, ensure_ascii=False).encode('utf-8')

    def device_id_options(self):
        access_token = request.headers.get('Authorization')
        access_token = access_token.split(' ')
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="ndhu",
            database="tempdb"
        )
        mycursor = mydb.cursor()
        sql = "SELECT * FROM token WHERE access_token = %s"
        temp = (access_token[1],)
        mycursor.execute(sql,temp)
        myresult = mycursor.fetchone()
        sql = "SELECT * FROM user WHERE id = %s"
        temp = (myresult[2],)
        mycursor.execute(sql,temp)
        myresult = mycursor.fetchone()
        sql = "SELECT * FROM data_setting WHERE email = %s"
        temp = (myresult[1],)
        mycursor.execute(sql,temp)
        myresult = mycursor.fetchall()
        datatemp = []
        for i in myresult:
            x={'label' : i[2],'value': i[2]}
            datatemp.append(x)
        mycursor.close()
        mydb.close()
        return json.dumps({'data': datatemp}, ensure_ascii=False).encode('utf-8')
