# coding: utf-8
from datetime import datetime, timedelta
from http_method_override import HTTPMethodOverride
from flask_caching import Cache
import logging
import sys
from flask import Flask
from flask import session, request, flash
from flask import render_template, redirect ,url_for, jsonify
from flask_login import UserMixin, LoginManager, current_user, login_required, login_user, logout_user
#from wtforms import Form, TextAreaField, validators, StringField, IntegerField
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import gen_salt, generate_password_hash, check_password_hash
from flask_oauthlib.provider import OAuth2Provider
import json
import urllib
import socket
import mysql.connector #mysql
from kafka import KafkaProducer
from confluent_kafka import Producer
import time
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.firefox.options import Options
from time import sleep
#import urllib.request
from werkzeug.utils import secure_filename

#producer = KafkaProducer(bootstrap_servers=['134.208.2.163:9092','134.208.2.163:9093','134.208.2.163:9094','134.208.2.163:9098','134.208.2.163:9097'])
                         #value_serializer=lambda x:
                         #dumps(x).encode('utf-8'))
conf = {'bootstrap.servers':"134.208.2.163:9092,134.208.2.163:9093,134.208.2.163:9094,134.208.2.163:9098,134.208.2.163:9097"}
producer = Producer(conf)

UPLOAD_FOLDER = '/home/hduser/Research/ssifttt/apiSifttt/upload'

app = Flask(__name__, template_folder='templates')
app.wsgi_app = HTTPMethodOverride(app.wsgi_app)
app.debug = True
app.secret_key = '3v\x9c1\xb1\xa6[\xd8\xae\x91%\x8a7\xf3.\x18i[u\x9d\xc8\x88\x13\x04'
app.config.update({
    'SQLALCHEMY_DATABASE_URI': 'mysql+mysqlconnector://root:ndhu@localhost:3306/tempdb',
})
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

db = SQLAlchemy(app)
oauth = OAuth2Provider(app)

login_manager = LoginManager()
login_manager.login_view = 'login'
login_manager.init_app(app)

IFTTT_SERVICE_KEY = "kpax4OO3o76S-wWt4u16KurI9DOBuOY_f0t08Bb-8LIxjauRmCM-ETGcHz7v804o"

from ssifttt import ssifttt as ssifttt_blueprint
app.register_blueprint(ssifttt_blueprint)

cache = Cache(config={'CACHE_TYPE': 'simple'})
cache.init_app(app)

root = logging.getLogger()
root.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    name = db.Column(db.String(150))
    password = db.Column(db.String(100))

class Client(db.Model):
    client_id = db.Column(db.String(40), primary_key=True)
    client_secret = db.Column(db.String(55), nullable=False)
    user_id = db.Column(db.ForeignKey('user.id'))
    user = db.relationship('User')
    _redirect_uris = db.Column(db.Text)
    _default_scopes = db.Column(db.Text)

    @property
    def client_type(self):
        return 'public'

    @property
    def redirect_uris(self):
        if self._redirect_uris:
            return self._redirect_uris.split()
        return []

    @property
    def default_redirect_uri(self):
        return self.redirect_uris[0]

    @property
    def default_scopes(self):
        if self._default_scopes:
            return self._default_scopes.split()
        return []

class Grantt(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    user = db.relationship('User')
    client_id = db.Column(db.String(40), db.ForeignKey('client.client_id'), nullable=False,)
    client = db.relationship('Client')
    code = db.Column(db.String(255), index=True, nullable=False)
    redirect_uri = db.Column(db.String(255))
    expires = db.Column(db.DateTime)
    _scopes = db.Column(db.Text)

    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self

    @property
    def scopes(self):
        if self._scopes:
            return self._scopes.split()
        return []

class Token(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.String(40), db.ForeignKey('client.client_id'), nullable=False,)
    client = db.relationship('Client')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id')    )
    user = db.relationship('User')
    # currently only bearer is supported
    token_type = db.Column(db.String(40))
    access_token = db.Column(db.String(255), unique=True)
    refresh_token = db.Column(db.String(255), unique=True)
    expires = db.Column(db.DateTime)
    _scopes = db.Column(db.Text)

    @property
    def scopes(self):
        if self._scopes:
            return self._scopes.split()
        return []


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login')
def login():
    if request.args.get('next') is not None:
        return render_template('login.html', next=request.args.get('next'))
    return render_template('login.html')

@app.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    temp = request.args.get('next')
    user = User.query.filter_by(email=email).first()
    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        if temp is not None:
            return redirect('/login?next=' + urllib.quote_plus(temp))
        else:
            return redirect(url_for('login'))

    #session['id'] = user.id
    login_user(user, remember=remember)
    return redirect(temp or url_for('profile'))

@app.route('/signup')
def signup():
    if request.args.get('next') is not None:
        return render_template('signup.html', next=request.args.get('next'))
    return render_template('signup.html')

@app.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    name = request.form.get('name')
    password = request.form.get('password')
    confirm_password = request.form.get('confirm_password')

    temp = request.args.get('next')
    if email == '' or name == '' or password == '' or confirm_password == '':
        flash('Each field can\'t be empty, please try again.')
        if temp is not None:
            return redirect('/signup?next=' + urllib.quote_plus(temp))
        else:
            return redirect(url_for('signup'))
    else:
        user = User.query.filter_by(email=email).first()
        if user:
            flash('Email address already exists, please try again.')
            if temp is not None:
                return redirect('/signup?next=' + urllib.quote_plus(temp))
            else:
                return redirect(url_for('signup'))
        elif password != confirm_password:
            flash('Confirm Password must be identical with Password, please try again.')
            if temp is not None:
                return redirect('/signup?next=' + urllib.quote_plus(temp))
            else:
                return redirect(url_for('signup'))

    new_user = User(email=email, name=name, password=generate_password_hash(password, method='sha256'))

    db.session.add(new_user)
    db.session.commit()

    return redirect(temp or url_for('login'))

@app.route('/profile')
@login_required
def profile():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="ndhu",
        database="tempdb"
    )
    mycursor = mydb.cursor(dictionary=True)
    sql = "SELECT * from data_setting WHERE email=%s"
    temp = (current_user.email,)
    mycursor.execute(sql,temp)
    data = mycursor.fetchall()
    mycursor.close()
    mydb.close()

    if data is not None:
        return render_template('profile.html', data=data, name=current_user.name)

    message = 'No Data Source Found'
    return render_template('profile.html', message=message, name=current_user.name)

@app.route('/add_datasource')
@login_required
def add_datasource():
    return render_template('add_datasource.html')

@app.route('/add_datasource', methods=['POST'])
@login_required
def add_datasource_post():
    data_id = request.form.get('data_id')
    description = request.form.get('description')
    ip = request.form.get('ip')
    port = request.form.get('port')

    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="ndhu",
        database="tempdb"
    )
    mycursor = mydb.cursor()
    if data_id == '':
        flash('Data ID can\'t be empty, please try again.')
        return redirect(url_for('add_datasource'))
    else:
        sql = "SELECT * FROM data_setting WHERE data_id=%s"
        temp = (data_id,)
        mycursor.execute(sql,temp)
        data = mycursor.fetchone()
        if data:
            flash('Data ID already exists, please try again.')
            mycursor.close()
            mydb.close()
            return redirect(url_for('add_datasource'))
        elif port != '':
            try:
                port = int(port)
            except:
                flash('Port number must be in numerical, please try again.')
                return redirect(url_for('add_datasource'))

    if port == '':
        port = 0
    sql = "SELECT name FROM trigger_pattern"
    mycursor.execute(sql)
    patterns = mycursor.fetchall()
    sql1 = "INSERT INTO data_setting (email,data_id,description,ip,port"
    sql2 = "VALUES (%s, %s, %s, %s, %s"
    temp = (current_user.email,data_id,description,ip,port)
    patternTemp = []
    for pattern in patterns:
        sql1 = sql1 + "," + pattern[0]
        sql2 = sql2 + ", %s"
        patternTemp.append(0)
    sql = sql1 + ") " + sql2 + ")"
    temp = temp + tuple(patternTemp)
    mycursor.execute(sql,temp)
    mydb.commit()

    mycursor.close()
    mydb.close()
    return redirect(url_for('profile'))

@app.route('/edit_datasource/<string:data_id>')
@login_required
def edit_datasource(data_id):
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="ndhu",
        database="tempdb"
    )
    mycursor = mydb.cursor(dictionary=True)
    sql = "SELECT * from data_setting WHERE data_id=%s"
    temp = (data_id,)
    mycursor.execute(sql,temp)
    data = mycursor.fetchone()
    mycursor.close()
    mydb.close()
    if current_user.email == data['email']:
        return render_template('edit_datasource.html', data=data)
    else:
        return redirect(url_for('profile'))

@app.route('/edit_datasource/<string:data_id>', methods=['POST'])
@login_required
def edit_datasource_post(data_id):
    description = request.form.get('description')
    ip = request.form.get('ip')
    port = request.form.get('port')

    if port != '':
        try:
            port = int(port)
        except:
            flash('Port number must be in numerical, please try again.')
            return redirect(url_for('edit_datasource', data_id=data_id))

    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="ndhu",
        database="tempdb"
    )
    mycursor = mydb.cursor()
    sql = "UPDATE data_setting SET description=%s, ip=%s ,port=%s WHERE data_id=%s"
    temp = (description, ip, port, data_id)
    mycursor.execute(sql,temp)
    mydb.commit()
    mycursor.close()
    mydb.close()
    return redirect(url_for('profile'))

@app.route('/delete_datasource/<string:data_id>', methods=['GET','POST'])
@login_required
def delete_datasource(data_id):
    # will check table that related to applet first
    # before can delete the datasource
    # if can't delete give flash message
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="ndhu",
        database="tempdb"
    )
    mycursor = mydb.cursor(dictionary=True)
    sql = "SELECT * from data_setting WHERE data_id=%s"
    temp = (data_id,)
    mycursor.execute(sql,temp)
    data = mycursor.fetchone()

    sql = "SELECT name from trigger_pattern"
    mycursor.execute(sql)
    patterns = mycursor.fetchall()

    flag = 0
    if current_user.email == data['email']:
        for pattern in patterns:
            if data[pattern['name']] != 0:
                flag = 1
                break;

        if flag == 0:
            sql = "DELETE FROM data_setting WHERE data_id=%s"
            temp = (data_id,)
            mycursor.execute(sql,temp)
            mydb.commit()
            mycursor.close()
            mydb.close()
            flash('Datasource deleted', 'is-info')
        else:
            flash('Please turn off all applet(s) related to data source "' + data_id + '" first.', 'is-danger')

    return redirect(url_for('profile'))

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/client')
@login_required
def client():
    item = Client(
        client_id=gen_salt(40),
        client_secret=gen_salt(50),
        _redirect_uris='https://ifttt.com/channels/sifttt/authorize',
        _default_scopes='ifttt',
        user_id=current_user.id,
    )
    db.session.add(item)
    db.session.commit()
    return jsonify(
        client_id=item.client_id,
        client_secret=item.client_secret,
    )

@oauth.clientgetter
def load_client(client_id):
    return Client.query.filter_by(client_id=client_id).first()

@oauth.grantgetter
def load_grant(client_id, code):
    return Grantt.query.filter_by(client_id=client_id, code=code).first()

@oauth.grantsetter
def save_grant(client_id, code, request, *args, **kwargs):
    # decide the expires time yourself
    expires = datetime.utcnow() + timedelta(seconds=100)
    grant = Grantt(
        client_id=client_id,
        code=code['code'],
        redirect_uri=request.redirect_uri,
        _scopes=' '.join(request.scopes),
        user=current_user,
        expires=expires
    )
    db.session.add(grant)
    db.session.commit()
    return grant

@oauth.tokengetter
def load_token(access_token=None, refresh_token=None):
    if access_token:
        return Token.query.filter_by(access_token=access_token).first()
    elif refresh_token:
        return Token.query.filter_by(refresh_token=refresh_token).first()

@oauth.tokensetter
def save_token(token, request, *args, **kwargs):
    toks = Token.query.filter_by(
        client_id=request.client.client_id,
        user_id=request.user.id
    )
    # make sure that every client has only one token connected to a user or two for ifttt
    #for t in toks[:-1]:    #for expiring token ifttt to ensure refresh token doesn't expire immediately
    for t in toks:          #non-expiring token
        db.session.delete(t)

    expires_in = token.pop('expires_in')
    expires = datetime.utcnow() + timedelta(seconds=expires_in)

    tok = Token(
        access_token=token['access_token'],
        refresh_token=token['refresh_token'],
        token_type=token['token_type'],
        _scopes=token['scope'],
        expires=expires,
        client_id=request.client.client_id,
        user_id=request.user.id,
    )
    db.session.add(tok)
    db.session.commit()
    return tok

@app.route('/oauth2/token', methods=['GET', 'POST'])
@oauth.token_handler
def access_token():
    return None

@app.route('/oauth2/authorize', methods=['GET', 'POST'])
@login_required
@oauth.authorize_handler
def authorize(*args, **kwargs):
    user = current_user
    if request.method == 'GET':
        client_id = kwargs.get('client_id')
        client = Client.query.filter_by(client_id=client_id).first()
        kwargs['client'] = client
        kwargs['user'] = user
        return render_template('authorize.html', **kwargs)

    confirm = request.form.get('confirm', 'no')
    if confirm == 'no':
        return redirect('https://ifttt.com/channels/sifttt/authorize?error=access_denied')
    return confirm == 'yes'

@app.route('/ifttt/v1/user/info')
#@oauth.require_oauth()
def user_info():
    access_token = request.headers.get('Authorization')
    access_token = access_token.split(' ')
    tempToken = Token.query.filter_by(access_token=access_token[1]).first()
    if tempToken is None:
        return json.dumps({'errors' : [{'message': 'Acess Token Denied!!!'}]}, ensure_ascii=False).encode('utf-8'), 401
    else:
        user_id = tempToken.user_id
        user = User.query.filter_by(id=user_id).first()
        return json.dumps({'data': {'name': user.name, 'id': user.email}}, ensure_ascii=False).encode('utf-8')

@app.route('/notifications', methods=['POST'])
def notifications():
    global producer
    req = request.json.get('data').encode('utf-8')
    print('req:',req)
    #req = str(req)
    req=req.replace("\'","\"")
    req=json.loads(req)
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="ndhu",
        database="tempdb"
    )
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    #validate user with data_id
    sql = "SELECT * FROM data_setting WHERE data_id=%s AND email=%s"
    temp = (req['id'],req['user_id'])
    mycursor.execute(sql,temp)
    userValidation = mycursor.fetchone()
    if userValidation is not None:
        if req['datatype']=='int':
            sql = "SELECT name FROM trigger_pattern"
            mycursor.execute(sql)
            patterns = mycursor.fetchall()
            data = dict()
            data['pattern'] = []

            sql = "SELECT * from data_setting WHERE data_id=%s"
            temp = (req['id'],)
            mycursor.execute(sql,temp)
            dataConf = mycursor.fetchone()

            for pattern in patterns:
                if dataConf[pattern['name']] != 0:
                    data['pattern'].append(pattern['name'])

            if len(data['pattern']) != 0:
                data['id'] = req['id']
                tempReq = dict()
                tempReq['type']='data'
                tempReq['value']=req['val']
                tempReq['time']=req['time']
                data['data']=tempReq
                data = json.dumps(data, ensure_ascii=False).encode('utf-8')
                #key=pattern[0].encode('utf-8')
                #if req[-1:] == '\n':
                #global producer
                #future=producer.send('sifttt1', value=data)
                #result=future.get(timeout=0)
                producer.produce('sifttt1',value=data)
                producer.poll(timeout=0)
    mycursor.close()
    mydb.close()
    return jsonify(''), 200

@app.route('/commands', methods=['POST'])
def commands():
    try:
        device_id = request.json['data']['id']
        latest_time = request.json['data']['latest_time']
    except:
        return json.dumps({'errors' : [{'status': 'SKIP', 'message': 'Missing Command Fields!!!'}]}, ensure_ascii=False).encode('utf-8'), 400

    commandTemp=[]
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="ndhu",
        database="tempdb"
    )
    mycursor = mydb.cursor()
    sql = "SELECT ip,port from data_setting WHERE data_id = %s"
    temp = (device_id,)
    mycursor.execute(sql,temp)
    temp = mycursor.fetchone()
    ip = temp[0]
    port = temp[1]
    sql = "SELECT command_message, created_at from command WHERE device_id = %s AND created_at > %s"
    temp = (device_id, latest_time)
    mycursor.execute(sql,temp)
    temp = mycursor.fetchall()
    for command_message,created_at in temp:
        created_at = created_at.isoformat()
        x={'device_id':device_id, 'command':command_message, 'ip':ip, 'port':port, 'created_at' : created_at}
        commandTemp.append(x)

    mycursor.close()
    mydb.close()
    return json.dumps({'data': commandTemp}, ensure_ascii=False).encode('utf-8')

ALLOWED_EXTENSIONS = set(['json', 'py'])

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload')
def upload_file():
	return render_template('upload.html')

@app.route('/upload', methods=['POST'])
def upload_file_post():
	if request.method == 'POST':
	# check if the post request has the files part
		if 'files[]' not in request.files:
			flash('No file part', 'is-danger')
			return redirect(request.url)
		files = request.files.getlist('files[]')
		for file in files:
			if file and allowed_file(file.filename):
				filename = secure_filename(file.filename)
				file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                if file.filename.split('.')[1] == 'json':
                    tempFilename = file.filename.replace(" ","_")
                    tempFile = 'upload/' + tempFilename
                    with open(tempFile, 'r') as f:
                        pattern = json.load(f)
                    try:
                        #db
                        mydb = mysql.connector.connect(
                          host="localhost",
                          user="root",
                          passwd="ndhu",
                          database="tempdb"
                        )
                        mycursor = mydb.cursor()
                        sql = "ALTER TABLE data_setting ADD " + pattern["endpoint"] + " INT"
                        mycursor.execute(sql)

                        sql = "UPDATE data_setting SET " + pattern["endpoint"] + "=0"
                        mycursor.execute(sql)
                        mydb.commit()

                        sql = "INSERT INTO trigger_pattern (name) VALUES (%s)"
                        temp = (pattern["endpoint"],)
                        mycursor.execute(sql,temp)
                        mydb.commit()

                        sql = "CREATE TABLE " + pattern["endpoint"]
                        sql = sql + "_setting (trigger_id VARCHAR(255) PRIMARY KEY, data_id VARCHAR(100),"
                        for triggerField in pattern["triggerFields"]:
                            sql = sql + triggerField["key"]
                            sql = sql + " " + triggerField["type"] + ","
                            sql2 = "INSERT INTO trigger_field (pattern, field) VALUES (%s, %s)"
                            temp = (pattern["endpoint"],triggerField["key"])
                            mycursor.execute(sql2,temp)
                            mydb.commit()
                        sql = sql + " created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP)"
                        mycursor.execute(sql)

                        mycursor.close()
                        mydb.close()

                        #IFTTT web
                        options=Options()
                        options.headless = True
                        browser = webdriver.Firefox(options=options, executable_path='./geckodriver')
                        browser.get("https://platform.ifttt.com/services/sifttt/triggers")

                        wait = WebDriverWait(browser, 30)
                        signin = wait.until(ec.presence_of_element_located((By.LINK_TEXT, 'sign in')))
                        signin.click()

                        username = wait.until(ec.presence_of_element_located((By.ID, 'user_username')))
                        password = browser.find_element_by_id('user_password')
                        username.send_keys("henrygunawan93")
                        password.send_keys("arsenal1913")
                        browser.find_element_by_xpath("//input[@type='submit' and @value='Sign in']").click()

                        wait.until(ec.visibility_of_element_located((By.LINK_TEXT, 'New trigger'))).click()
                        triggerName = wait.until(ec.presence_of_element_located((By.ID, 'partners_trigger_name')))
                        triggerDesc = browser.find_element_by_id('partners_trigger_description')
                        triggerUrl = browser.find_element_by_id('partners_trigger_slug')
                        triggerName.send_keys(str(pattern["name"]))
                        triggerDesc.send_keys(str(pattern["description"]))
                        triggerUrl.send_keys(str(pattern["endpoint"]))
                        browser.find_element_by_xpath("//button[@name='button' and @type='submit']").click()

                        wait.until(ec.visibility_of_element_located((By.XPATH, "//a[@title='Edit ingredient']"))).click()
                        select_element = wait.until(ec.visibility_of_element_located((By.XPATH, "//select[option[@value='string']]")))
                        select = Select(select_element)
                        select.select_by_visible_text('String')
                        createdAtExample = wait.until(ec.presence_of_element_located((By.ID, 'partners_trigger_ingredient_example')))
                        createdAtExample.send_keys("2019-04-11 14:00:00")
                        browser.find_element_by_css_selector('button.btn.btn-primary.btn-sm').click()
                        sleep(2)

                        wait.until(ec.visibility_of_element_located((By.XPATH, "//input[@name='commit' and @value='Add trigger field +']"))).click()
                        triggerFieldName = wait.until(ec.presence_of_element_located((By.ID, 'partners_trigger_field_name')))
                        triggerFieldHelperText = browser.find_element_by_id('partners_trigger_field_helper_text')
                        triggerFieldKey = browser.find_element_by_id('partners_trigger_field_slug')
                        triggerFieldName.send_keys("Data Source")
                        triggerFieldHelperText.send_keys("Data Source ID")
                        triggerFieldKey.send_keys(Keys.CONTROL + "a")
                        triggerFieldKey.send_keys(Keys.DELETE)
                        triggerFieldKey.send_keys("data_id")
                        select_element = browser.find_element_by_xpath("//select[option[@value='dropdown_field_type']]")
                        select = Select(select_element)
                        select.select_by_visible_text('Dropdown list')
                        sleep(1)
                        select_element = browser.find_element_by_xpath("//select[option[@data-item-id='6']]")
                        select = Select(select_element)
                        select.select_by_index(1)
                        browser.find_element_by_css_selector('button.btn.btn-primary.btn-sm').click()

                        for triggerField in pattern["triggerFields"]:
                            wait.until(ec.visibility_of_element_located((By.XPATH, "//input[@name='commit' and @value='Add trigger field +']"))).click()
                            triggerFieldName = wait.until(ec.presence_of_element_located((By.ID, 'partners_trigger_field_name')))
                            triggerFieldHelperText = browser.find_element_by_id('partners_trigger_field_helper_text')
                            triggerFieldKey = browser.find_element_by_id('partners_trigger_field_slug')
                            triggerFieldName.send_keys(str(triggerField["label"]))
                            triggerFieldHelperText.send_keys(str(triggerField["helperText"]))
                            triggerFieldKey.send_keys(Keys.CONTROL + "a")
                            triggerFieldKey.send_keys(Keys.DELETE)
                            triggerFieldKey.send_keys(str(triggerField["key"]))
                            browser.find_element_by_css_selector('button.btn.btn-primary.btn-sm').click()

                        wait.until(ec.visibility_of_element_located((By.XPATH, "//input[@name='commit' and @value='Add ingredient +']"))).click()
                        ingredientName = wait.until(ec.presence_of_element_located((By.ID, 'partners_trigger_ingredient_name')))
                        ingredientSlug = browser.find_element_by_id('partners_trigger_ingredient_slug')
                        ingredientNote = browser.find_element_by_id('partners_trigger_ingredient_note')
                        ingredientExample = browser.find_element_by_id('partners_trigger_ingredient_example')
                        ingredientName.send_keys("DataSourceId")
                        ingredientSlug.send_keys(Keys.CONTROL + "a")
                        ingredientSlug.send_keys(Keys.DELETE)
                        ingredientSlug.send_keys("data_id")
                        ingredientNote.send_keys("Identity of data source.")
                        ingredientExample.send_keys("WA")
                        browser.find_element_by_css_selector('button.btn.btn-primary.btn-sm').click()
                        sleep(2)

                        verbiage = wait.until(ec.presence_of_element_located((By.XPATH, "//textarea[@name='partners_trigger[verbiage]']")))
                        verbiage.send_keys(str(pattern["verbiage"]))
                        browser.find_element_by_xpath("//input[@type='submit' and @name='commit' and @value='Save']").click()
                        sleep(2)
                        browser.close()
                    except:
                        flash('Failed to create new Pattern! Pay attention to all the rule to create a new pattern and make sure the pattern name is not exist.', 'is-danger')
                        return redirect(url_for('profile'))
		flash('File(s) successfully uploaded.', 'is-info')
		return redirect(url_for('profile'))

@app.route('/api/me')
@oauth.require_oauth()
def me(req):
    user = req.user
    return jsonify(username=user.username)

if __name__ == '__main__':
    db.create_all()
    app.run(threaded=True)
