import time
import json
import requests
import uuid
import mysql.connector #mysql
from datetime import datetime

IFTTT_SERVICE_KEY = "kpax4OO3o76S-wWt4u16KurI9DOBuOY_f0t08Bb-8LIxjauRmCM-ETGcHz7v804o"

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="ndhu",
    database="tempdb"
)
mycursor = mydb.cursor()
mycursor.execute("SELECT MAX(created_at) FROM events")
temp = mycursor.fetchone()
latestEvent = temp[0]
mycursor.close()
mydb.close()
while True:
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="ndhu",
        database="tempdb"
    )
    mycursor = mydb.cursor()
    mycursor.execute("SELECT MAX(created_at) FROM events")
    EventTime = mycursor.fetchone()

    if latestEvent < EventTime[0]:
        #print('latest:',latestEvent)
        #print('event time:',EventTime[0])
        newTriggerId = []
        sql = "SELECT DISTINCT trigger_id FROM events WHERE created_at > %s"
        temp = (latestEvent,)
        mycursor.execute(sql,temp)
        newTriggers = mycursor.fetchall()
        latestEvent = EventTime[0]
        #print('latestss:',latestEvent)
        url = 'https://realtime.ifttt.com/v1/notifications'
        uuidTemp = str(uuid.uuid4())
        headers = {'IFTTT-Service-Key': IFTTT_SERVICE_KEY,
                    'Accept': 'application/json',
                    'Accept-Charset': 'utf-8',
                    'Accept-Encoding': 'gzip, deflate',
                    'Content-Type': 'application/json',
                    'X-Request-ID': uuidTemp}
        for newTrigger in newTriggers:
            x={'trigger_identity' : str(newTrigger[0])}
            newTriggerId.append(x)
        data = json.dumps({'data': newTriggerId}, ensure_ascii=False).encode('utf-8')
        print('time1: ',datetime.now().isoformat())
        res = requests.post(url, headers=headers, data=data)
        print('time2: ',datetime.now().isoformat())
        print('response: '+str(res))
    time.sleep(1)
    mycursor.close()
    mydb.close()
