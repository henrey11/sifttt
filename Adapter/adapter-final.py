import socket
import time
import json
import requests
from flask import jsonify
import multiprocessing
import datetime

#socket to receive data from sensor
HOST = '192.168.1.192' #localhost
PORT = 8080         #Port send # TODO:

socRecv = socket. socket(socket.AF_INET, socket.SOCK_DGRAM)
socRecv.bind((HOST, PORT))

#socket to send data to device
HOST = '192.168.1.192' #localhost
PORT = 8081         #Port send # TODO:

socSend = socket. socket(socket.AF_INET, socket.SOCK_DGRAM)
socSend.bind((HOST, PORT))

user_id = "ca@gmail.com"
datatype = "int"

def dataCollector(socRecv):
    url = 'https://e41f3819.ngrok.io/notifications'

    while True:
        print('Waiting to receive...')
        recvData, addr = socRecv.recvfrom(1024)

        headers = {'Accept': 'application/json',
                    'Accept-Charset': 'utf-8',
                    'Accept-Encoding': 'gzip, deflate',
                    'Content-Type': 'application/json'}
        print('recv:',recvData)

        recvData = recvData.split(':')
        eventTime = datetime.datetime.now()
        eventTime = eventTime.isoformat()
        temp = str({'id':str(recvData[0]),'val': float(recvData[1]),'user_id':user_id,'datatype':datatype,'time':eventTime})
        #temp = temp + '\n'
        data = json.dumps({'data':temp}, ensure_ascii=False).encode('utf-8')
        res = requests.post(url, headers=headers, data=data)


def actorPolling(socSend, id,sec):
    url = 'https://e41f3819.ngrok.io/commands'
    headers = {'Accept': 'application/json',
                'Accept-Charset': 'utf-8',
                'Accept-Encoding': 'gzip, deflate',
                'Content-Type': 'application/json'}
    latestTime = datetime.datetime.now()
    latestTime = latestTime.isoformat()

    while True:
        tempHb = {'id':id,'latest_time':latestTime}
        data = json.dumps({'data':tempHb}, ensure_ascii=False).encode('utf-8')
        res = requests.post(url, headers=headers, data=data)
        print('response:',res.text)
        #if res.text is not None or res.text != '':
        res = json.loads(str(res.text))
        for i in res['data']:
            #print(i)
            #print('command:', i['command'])
            temp = 'COMMAND#' + str(i['command'])
            socSend.sendto(temp, (str(i['ip']), int(i['port'])))
            latestTime = i['created_at']
        time.sleep(sec)

if __name__ == "__main__":

    multiprocessing.Process(target=dataCollector, args=(socRecv,)).start()
    #multiprocessing.Process(target=actorPolling, args=(socSend,'WA',3)).start()
    multiprocessing.Process(target=actorPolling, args=(socSend,'TEMP',1)).start()
