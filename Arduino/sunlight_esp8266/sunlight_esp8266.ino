#include <TimerOne.h>
#include <SoftwareSerial.h>
#include <Wire.h>


#include "Arduino.h"
#include "SI114X.h"

#define RX 10
#define TX 11

SoftwareSerial wifiSerial(RX,TX);
SI114X SI1145 = SI114X();

bool DEBUG = true;   //show more logs
const int redPin = 3;
const int greenPin = 5;
const int bluePin = 6;

volatile unsigned int ct = 0;

void setup() {
 Serial.begin(9600);
 wifiSerial.begin(9600);
 sendToWifi("AT+CWMODE=3\r\n",1000,DEBUG);//3
 sendToWifi("AT+CWJAP=\"PCDM\",\"pcdm3401\"\r\n",10000,DEBUG);
 sendToWifi("AT+CIFSR\r\n",2000,DEBUG);
 sendToWifi("AT+CIPMUX=1\r\n",1000,DEBUG);
 sendToWifi("AT+CIPSTART=4,\"UDP\",\"192.168.1.192\",8080,1112,0\r\n",3000,DEBUG);
 //sendToWifi("AT+CIPSEND=4,7\r\n",1000,DEBUG);
 //sendToWifi("UDPtest\r\n",1000,DEBUG);
 pinMode(redPin, OUTPUT);
 pinMode(greenPin, OUTPUT);
 pinMode(bluePin, OUTPUT);
 Serial.println("Beginning Si1145!");
 while (!SI1145.Begin()) {
   Serial.println("Si1145 is not ready!");
   delay(1000);
 }
 Serial.println("Si1145 is ready!");
 Timer1.initialize(1000000); // set a timer of length 1000000 microseconds
 Timer1.attachInterrupt( timerIsr );
} 

void timerIsr()
{
  ct++;
}

int readVisible(){
  return SI1145.ReadVisible();
  //return SI1145.ReadIR();
  //return (float)SI1145.ReadUV()/100;
}

void commandControl(){
  if(wifiSerial.available()) // check if the esp is sending a message 
      {
        int index = 0;
        String buf;
        long int time = millis();
        
        while( (time+50) > millis())
        {
          while(wifiSerial.available())
          {
            // The esp has data so display its output to the serial window 
            char c = wifiSerial.read(); // read the next character.
            buf += c;
          } 
        }
        Serial.print("buffer: ");
        Serial.print(buf);
        Serial.println(" ENDBUFFER");
        index = buf.indexOf('COMMAND#');
        /*
        if(buf.substring(index+1, index+4) == "VIS"){
          int vis = readVisible();
          String atSend = "AT+CIPSEND=4,";
          String atData = "TP:";
          atData += vis;
          //atData += "\r\n";
          atSend += atData.length();
          atSend += "\r\n";
          sendToWifi(atSend,50,DEBUG);
          sendToWifi(atData,50,DEBUG);
        }
        else*/
        if(buf.substring(index+1, index+6) == "REDON"){
          digitalWrite(greenPin,LOW);
          digitalWrite(bluePin,LOW);
          digitalWrite(redPin,HIGH);
        }
        else
        if(buf.substring(index+1, index+7) == "REDOFF"){
          digitalWrite(redPin,LOW);
        }
        else
        if(buf.substring(index+1, index+8) == "GREENON"){
          digitalWrite(redPin,LOW);
          digitalWrite(bluePin,LOW);
          digitalWrite(greenPin,HIGH);
        }
        else
        if(buf.substring(index+1, index+9) == "GREENOFF"){
          digitalWrite(greenPin,LOW);
        }
        else
        if(buf.substring(index+1, index+7) == "BLUEON"){
          digitalWrite(redPin,LOW);
          digitalWrite(greenPin,LOW);
          digitalWrite(bluePin,HIGH);
        }
        else
        if(buf.substring(index+1, index+8) == "BLUEOFF"){
          digitalWrite(bluePin,LOW);
        }
        else
        if(buf.substring(index+1, index+8) == "WHITEON"){
          digitalWrite(redPin,HIGH);
          digitalWrite(greenPin,HIGH);
          digitalWrite(bluePin,HIGH);
        }
        else
        if(buf.substring(index+1, index+9) == "WHITEOFF"){
          digitalWrite(redPin,LOW);
          digitalWrite(greenPin,LOW);
          digitalWrite(bluePin,LOW);
        }
  }
}

void loop() {
  commandControl();
  if (ct == 3){
    int vis = readVisible();
    String atSend = "AT+CIPSEND=4,";
    String atData = "LIGHT:";
    atData += vis;
    //atData += "\r\n";
    atSend += atData.length();
    atSend += "\r\n";
    sendToWifi(atSend,50,DEBUG);
    sendToWifi(atData,50,DEBUG);
    ct=0;
  }
}

String sendToWifi(String command, const int timeout, boolean debug){
  String response = "";
  wifiSerial.print(command); // send the read character to the esp8266
  long int time = millis();
  while( (time+timeout) > millis())
  {
    while(wifiSerial.available())
    {
    // The esp has data so display its output to the serial window 
    char c = wifiSerial.read(); // read the next character.
    response+=c;
    }  
  }
  if(debug)
  {
    Serial.println(response);
  }
  return response;
}
