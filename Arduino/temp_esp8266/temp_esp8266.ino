#include <TimerOne.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include <AM2320.h>

#define RX 10
#define TX 11

SoftwareSerial wifiSerial(RX,TX);
AM2320 sensor;

int segPins[] = {8,2,3,4,5,6,7,12};
bool DEBUG = true;   //show more logs

const int numOfDigits=1;
int digitPins[numOfDigits]={12};
volatile unsigned int ct = 0;

byte segCode[11][8] = {
//  a  b  c  d  e  f  g  .
  { 0, 0, 0, 0, 0, 0, 1, 0},  // 0
  { 1, 0, 0, 1, 1, 1, 1, 0},  // 1
  { 0, 0, 1, 0, 0, 1, 0, 0},  // 2
  { 0, 0, 0, 0, 1, 1, 0, 0},  // 3
  { 1, 0, 0, 1, 1, 0, 0, 0},  // 4
  { 0, 1, 0, 0, 1, 0, 0, 0},  // 5
  { 0, 1, 0, 0, 0, 0, 0, 0},  // 6
  { 0, 0, 0, 1, 1, 1, 1, 0},  // 7
  { 0, 0, 0, 0, 0, 0, 0, 0},  // 8
  { 0, 0, 0, 0, 1, 0, 0, 0},  // 9
  { 1, 1, 1, 1, 1, 1, 1, 0}   // .
};

void setup() {
 Serial.begin(9600);
 wifiSerial.begin(9600);
 sendToWifi("AT+CWMODE=3\r\n",1000,DEBUG);//3
 sendToWifi("AT+CWJAP=\"PCDM\",\"pcdm3401\"\r\n",10000,DEBUG);
 sendToWifi("AT+CIFSR\r\n",2000,DEBUG);
 sendToWifi("AT+CIPMUX=1\r\n",1000,DEBUG);
 sendToWifi("AT+CIPSTART=4,\"UDP\",\"192.168.1.192\",8080,1112,0\r\n",3000,DEBUG);
 //sendToWifi("AT+CIPSEND=4,7\r\n",1000,DEBUG);
 //sendToWifi("UDPtest\r\n",1000,DEBUG);
 for (int i=0; i < 8; i++)
  {
    pinMode(segPins[i], OUTPUT);
  }
 displayDigit(10);
 Timer1.initialize(1000000); // set a timer of length 1000000 microseconds
 Timer1.attachInterrupt( timerIsr );
}

void displayDigit(int digit)
{
  for (int i=0; i < 7; i++)
  {
    digitalWrite(segPins[i], segCode[digit][i]);
  }
}

void timerIsr()
{
  ct++;
}

float readTemperature(){
  switch(sensor.Read()) {
    case 2:
      Serial.println("CRC failed");
      break;
    case 1:
      Serial.println("Sensor offline");
      break;
    case 0:
      //Serial.println(sensor.t);
      return sensor.t;
      //return sensor.h;
  }
}

void commandControl(){
  if(wifiSerial.available()) // check if the esp is sending a message 
      {
        int index = 0;
        String buf;
        long int time = millis();
        
        while( (time+50) > millis())
        {
          while(wifiSerial.available())
          {
            // The esp has data so display its output to the serial window 
            char c = wifiSerial.read(); // read the next character.
            buf += c;
          } 
        }
        Serial.print("buffer: ");
        Serial.print(buf);
        Serial.println(" ENDBUFFER");
        index = buf.indexOf('COMMAND#');
        /*
        if(buf.substring(index+1, index+5) == "TEMP"){
          float temp = readTemperature();
          String atSend = "AT+CIPSEND=4,";
          String atData = "WA:";
          atData += temp;
          //atData += "\r\n";
          atSend += atData.length();
          atSend += "\r\n";
          sendToWifi(atSend,50,DEBUG);
          sendToWifi(atData,50,DEBUG);
        }
        else*/
        int temp = (buf.substring(index+1, index+2)).toInt();
        if((temp >= 0) && (temp < 10)){
          //disp.write(temp);
          displayDigit(temp);
        }
  }
}

void loop() {
  commandControl();
  if (ct == 3){
    float temp = readTemperature();
    String atSend = "AT+CIPSEND=4,";
    String atData = "TEMP:";
    atData += temp;
    //atData += "\r\n";
    atSend += atData.length();
    atSend += "\r\n";
    sendToWifi(atSend,50,DEBUG);
    sendToWifi(atData,50,DEBUG);
    ct=0;
  }
}

String sendToWifi(String command, const int timeout, boolean debug){
  String response = "";
  wifiSerial.print(command); // send the read character to the esp8266
  long int time = millis();
  while( (time+timeout) > millis())
  {
    while(wifiSerial.available())
    {
    // The esp has data so display its output to the serial window 
    char c = wifiSerial.read(); // read the next character.
    response+=c;
    }  
  }
  if(debug)
  {
    Serial.println(response);
  }
  return response;
}
